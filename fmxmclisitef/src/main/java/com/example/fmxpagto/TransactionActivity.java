package com.example.fmxpagto;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.genexusmodule.R;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Random;

import br.com.gertec.epwp.Main;
import br.com.gertec.gedi.GEDI;
import br.com.gertec.gedi.exceptions.GediException;
import br.com.gertec.gedi.interfaces.IGEDI;
import br.com.gertec.gedi.interfaces.IPRNTR;
import br.com.gertec.gedi.structs.GEDI_PRNTR_st_StringConfig;
import br.com.softwareexpress.sitef.android.CliSiTef;
import br.com.softwareexpress.sitef.android.ICliSiTefListener;
import wangpos.sdk4.libbasebinder.Printer;

public class TransactionActivity extends Activity implements ICliSiTefListener {
    //-- Campos do Sitef
    private static final int CAMPO_COMPROVANTE_CLIENTE = 121;
    private static final int CAMPO_COMPROVANTE_ESTAB = 122;
    private static final int CAMPO_NSU_SITEF = 133;
    private static final int CAMPO_NSU_HOST = 134;
    private static final int CAMPO_COD_AUTORIZACAO_CREDITO = 135;
    private static final int CAMPO_CARTAO_INICIO_NUM = 136;
    private static final int CAMPO_COD_REDE_AUTORIZADORA = 158;
    private static final int CAMPO_CARTAO_VALIDADE = 1002;
    private static final int CAMPO_NOME_PORTADOR = 1003;
    private static final int CAMPO_CARTAO_FINAL_NUM = 1190;
    private static final int CAMPO_CARTAO_MASCARADO = 2021;
    private static final int CAMPO_COLETOU_SENHA_PINPAD = 5074;

    //-- Classes de impressão
    IGEDI iGedi;
    IPRNTR iprntr;

    //-- Parametros globais
    private File applicationPath = null;
    private String g_ip = "";
    private String g_empresa = "";
    private String g_cnpjLoja = "";
    private String g_equipamento = deviceId();
    private int g_funcao = 0;
    private String g_parametros = "";

    // Parametros das Pré Reserva
    private String p_valor = "";
    private String g_data = "";
    private String g_hora = "";
    private String p_operador = "";
    private String g_cupom = "";

    //-- Parametro do Cancelamento da Pré Autorização
    private String cp_valor = "";
    private String cg_data = "";
    private String cp_nsu = "";
    private String cp_autorizacao = "";


    private class RequestCode {
        private static final int GET_DATA = 1;
        private static final int END_STAGE_1_MSG = 2;
        private static final int END_STAGE_2_MSG = 3;
    }

    //-- Inicialização das variváveis globais
    private Printer mPrinter;
    private String  mPrintedData = "";

    private String aditionalData = "";
    private String impressao="";
    private int QtdErros=0;
    private int vReturn;
    private boolean erro = true;

    private int trnResultCode;
    private static String title;
    private static CliSiTef clisitef;
    private static TransactionActivity instance = null;


    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_transaction);

        GEDI.init(TransactionActivity.this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                GEDI.init(getApplicationContext());
                iGedi = GEDI.getInstance(getApplicationContext());
            }
        }).start();


        verificaParametros();

        Button btn = (Button) findViewById(R.id.btCfgCancela);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (clisitef.abortTransaction(-1) != 0) {
                    // Se não há o que abortar, encerra a Activity
                    finish();
                }
            }
        });

        Log.e("LOH", g_equipamento);

        instance = this;

        if (clisitef == null) {
            try {
                clisitef = new CliSiTef(this.getApplicationContext());
            } catch (Exception e) {
                //alert(e.getMessage());
            }
        }

        clisitef.setDebug(true);
        //int S = clisitef.configure("192.168.0.3", "00000000", g_equipamento, "[ParmsClient=1=31328635000101;2=04437534000130;];[TipoPinPad=ANDROID_AUTO;]");
        //String paramLoja = "[ParmsClient=1=" + g_cnpjLoja.trim() + ";2=04437534000130]";

        Log.e("PARAM", g_ip);
        Log.e("PARAM", g_empresa);
        Log.e("PARAM", g_equipamento);
        Log.e("PARAM", g_parametros);

        applicationPath = getApplicationContext().getFilesDir();
        String parsedParam = g_parametros.replace("#ANDROID_APLICACAO", applicationPath.getAbsolutePath() + "/");
        Log.e("PARSED", parsedParam);
        int S = clisitef.configure(g_ip, g_empresa, g_equipamento, parsedParam);

        if (S == 0) {
            Log.d("LOG","Configurado");
        }

        clisitef.setMessageHandler(hndMessage);
        trnResultCode = -1; // undefined
        title = "";
        setStatus("");
        clisitef.setActivity(this);
        //g_funcao = 3; // 115 Pre / 116 Confirmação // 202 Cancelamento
        //vReturn = clisitef.startTransaction (this, g_funcao, "1222,00", "123456", "23/03/2021", "11:10", "GUSTAVO", "[]");

        Log.e("ValorInicio", g_cupom);
        vReturn = clisitef.startTransaction (this, g_funcao, p_valor, g_cupom, g_data, g_hora, p_operador, "[]");

        if (g_funcao==114) {
            btn.setVisibility(View.GONE);
            setStatus("Imprimindo...");
        }

        Log.e("LOG:", "Transaction: " + vReturn);
    }

    protected void onDestroy() {
        instance = null;
        super.onDestroy();
    }

    private void setStatus(String s) {
        ((TextView) findViewById(R.id.tvStatusTrn)).setText(s);
    }

    private void alert(String message) {
        Toast.makeText(TransactionActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    public void onData(int stage, int command, int fieldId, int minLength, int maxLength, byte[] input) {

        String data = "";
        Button btn = (Button) findViewById(R.id.btCfgCancela);
        setProgressBarIndeterminateVisibility(false);

        if (stage == 1) {
            // Evento onData recebido em uma startTransaction
        } else if (stage == 2) {
            // Evento onData recebido em uma finishTransaction
        }

        String mensagem = clisitef.getBuffer();

        Log.e("LOG:", " fieldId: " + fieldId + " stage: " + stage + " command: "+ command + " ondata: " + clisitef.getBuffer());


        switch (command) {
            case CliSiTef.CMD_GET_PINPAD_CONFIRMATION:
                break;
            case CliSiTef.CMD_RESULT_DATA:
                switch (fieldId) {
                    case CAMPO_COMPROVANTE_CLIENTE:
                        if (g_funcao==114) {
                            btn.setVisibility(View.GONE);
                            setStatus("Imprimindo...");
                            impressao = clisitef.getBuffer();

                            impressao = "Via Cliente" + System.getProperty("line.separator") +  impressao;

                            Log.e("Impressao", "Antes de chamar");
                            printResult(impressao);
                        }
                        break;

                    case CAMPO_COMPROVANTE_ESTAB:
                        btn.setVisibility(View.GONE);
                        setStatus("Imprimindo...");
                        impressao = clisitef.getBuffer();
                        if (g_funcao==114) {
                            impressao = "Via Cliente" + System.getProperty("line.separator") +  impressao;
                        }
                        Log.e("Impressao", "Antes de chamar");
                        printResult(impressao);
                        break;

                    case  CAMPO_COD_AUTORIZACAO_CREDITO:
                        aditionalData += "COD_AUTORIZACAO_CREDITO:" + mensagem + ";";
                        break;
                    case  CAMPO_CARTAO_INICIO_NUM:
                        aditionalData += "CARTAO_INICIO_NUM:" + mensagem + ";";
                        break;
                    case  CAMPO_COD_REDE_AUTORIZADORA:
                        aditionalData += "COD_REDE_AUTORIZADORA:" + mensagem + ";";
                        break;
                    case  CAMPO_CARTAO_VALIDADE:
                        aditionalData += "CARTAO_VALIDADE:" + mensagem + ";";
                        break;
                    case  CAMPO_NOME_PORTADOR:
                        aditionalData += "NOME_PORTADOR:" + mensagem + ";";
                        break;
                    case  CAMPO_CARTAO_FINAL_NUM:
                        aditionalData += "CARTAO_FINAL_NUM:" + mensagem + ";";
                        break;
                    case  CAMPO_CARTAO_MASCARADO:
                        aditionalData += "CARTAO_MASCARADO:" + mensagem + ";";
                        break;
                    case  CAMPO_NSU_SITEF:
                        aditionalData += "NSU_SITEF:" + mensagem + ";";
                        break;
                    case  CAMPO_NSU_HOST:

                        aditionalData += "NSU_HOST:" + mensagem + ";";
                        break;
                    case  CAMPO_COLETOU_SENHA_PINPAD:
                        switch (mensagem.trim()){
                            case "1":
                                returnData("ERRO: Senha não informada, operação cancelada");
                                break;
                            case "0":
                                btn.setVisibility(View.GONE);
                                break;
                        }

                        break;
                }
                break;


            case CliSiTef.CMD_SHOW_MSG_CASHIER:
            case CliSiTef.CMD_SHOW_MSG_CUSTOMER:
            case CliSiTef.CMD_SHOW_MSG_CASHIER_CUSTOMER:
                setStatus(clisitef.getBuffer());
                break;
            case CliSiTef.CMD_SHOW_MENU_TITLE:
            case CliSiTef.CMD_SHOW_HEADER:
                title = clisitef.getBuffer();
                break;
            case CliSiTef.CMD_CLEAR_MSG_CASHIER:
            case CliSiTef.CMD_CLEAR_MSG_CUSTOMER:
            case CliSiTef.CMD_CLEAR_MSG_CASHIER_CUSTOMER:
            case CliSiTef.CMD_CLEAR_MENU_TITLE:
            case CliSiTef.CMD_CLEAR_HEADER:
                title = "";
                setStatus("");
                break;
            case CliSiTef.CMD_CONFIRM_GO_BACK:
            case CliSiTef.CMD_CONFIRMATION: {
                switch(mensagem.trim()){
                    case "Conf.reimpressao":
                        clisitef.continueTransaction("0");
                        break;
                    case "41 - Erro de Leitura. Tentar novamente?":
                        clisitef.continueTransaction("0");
                        break;
                    case "13 - Operacao Cancelada?":
                        clisitef.continueTransaction("-1");
                        break;

                }
                if (mensagem.matches("Enviar.*arquivo.*\\?")){
                    clisitef.continueTransaction("0");
                }

                return;
            }
            case CliSiTef.CMD_GET_FIELD_CURRENCY:
                Log.e("CMD_GET_FIELD_CURRENCY", "CMD_GET_FIELD_CURRENCY");
                switch(mensagem.trim()) {
                    case "Forneca o valor da Pre-autorizacao":
                        Log.e("Pre-autorizacao", p_valor);
                        clisitef.continueTransaction(p_valor);
                        break;
                    case "Digite o valor da transacao":
                        Log.e("valor da transacao", cg_data);
                        clisitef.continueTransaction(cp_valor);
                        break;
                    default:
                        Log.e("CMD_GET_FIELD_CURRENCY", "default");
                        clisitef.continueTransaction("");
                        break;
                }

            case CliSiTef.CMD_GET_FIELD_BARCODE:
            case CliSiTef.CMD_GET_FIELD: {
                switch (mensagem.trim()) {
                    case "Forneca o codigo do supervisor":
                        clisitef.continueTransaction("1");
                        break;
                    case "Forneca os 4 digitos finais do cartao":
                        solicitaInformacao(mensagem);
                        break;
                    case "Codigo de Seguranca do cartao":
                        solicitaInformacao(mensagem);
                        break;
                    case "Data da transacao (DDMMAAAA)":
                        Log.e("Data da transacao", cg_data);
                        clisitef.continueTransaction(cg_data);
                        break;
                    case "Forneca o numero do documento a ser cancelado":
                        clisitef.continueTransaction(cp_nsu);
                        break;
                    case "Forneca o codigo de autorizacao":
                        clisitef.continueTransaction(cp_autorizacao);
                        break;

                }

                return;
            }
            case CliSiTef.CMD_GET_MENU_OPTION: {


                switch (mensagem.trim()){
                    case "1:CREDITO;2:PRIVATE LABEL;3:PRIVATE LABEL;":
                        clisitef.continueTransaction("1");
                        return;
                    case "1:Cheque;2:Cartao de Debito;3:Cartao de Credito;4:Cartao Private Label;5:Confirmacao de Pre-autorizacao;":
                        clisitef.continueTransaction("3");
                        return;
                    case "1:A Vista;2:Parcelado pelo Estabelecimento;3:Parcelado pela Administradora;4:Consulta parcelamento;":
                        clisitef.continueTransaction("1");
                        return;
                    case "1:A Vista;2:Parcelado pelo Estabelecimento;":
                        clisitef.continueTransaction("1");
                        return;
                    case "1:A Vista;2:Financ. Loja;3:Financ. Adm.;":
                        clisitef.continueTransaction("1");
                        return;

                }


            }
            case CliSiTef.CMD_PRESS_ANY_KEY: {
                switch (mensagem.trim()){
                    case "42 - Erro Pinpad":
                        returnData("ERRO: " + mensagem);
                        return;
                    case "43 - Cartao Removido":
                        returnData("ERRO: " + mensagem);
                        return;
                    case "12 - Erro Pinpad":
                        returnData("ERRO: " + mensagem);
                        return;
                    case "13 - Operacao Cancelada":
                        returnData("ERRO: " + mensagem);
                        return;
                    case "Cartao com chip. Insira o cartao":
                        QtdErros++;
                        if(QtdErros >= 3){
                            returnData("ERRO: " + "Não foi possível ler os dados do cartão.");
                            return;
                        }

                }
                clisitef.continueTransaction("1");
                return;
            }
            case CliSiTef.CMD_ABORT_REQUEST:
                break;

            case 50:
                break;

            case 51:
                break;
            default:
                break;
        }


        setProgressBarIndeterminateVisibility(true);
        clisitef.continueTransaction(data);
    }

    private String getMessageDescription(int stage, int sts) {
        switch (sts) {
            case -1:
                return "ERRO:" + getString(R.string.msgModuloNaoConfigurado);
            case -2:
                return("ERRO:" + getString(R.string.msgCanceladoOperador));
            case -3:
                return( "ERRO:" + getString(R.string.msgFuncaoInvalida));
            case -4:
                return( "ERRO:" + getString(R.string.msgFaltaMemoria));
            case -5:
                return( "ERRO:" + getString(R.string.msgFalhaComunicacao));
            case -6:
                return( "ERRO:" + getString(R.string.msgCanceladoPortador));
            case -40:
                return( "ERRO:" + getString(R.string.msgNegadaSiTef));
            case -43:
                return( "ERRO: Generic"); //getString(R.string.msgErroPinPad);
            case -100:
                return( "ERRO:" + getString(R.string.msgOutrosErros));
            default:
                return( "ERRO:" + "Stage " + stage + getString(R.string.msgReturned) + " " + sts);
        }
    }

    public void onTransactionResult(int stage, int resultCode) {
        setProgressBarIndeterminateVisibility(false);
        trnResultCode = resultCode;
        Log.e("onTransactionResult", String.valueOf(stage) + " - " + String.valueOf(resultCode));
        if (stage == 1 && resultCode == 0) { // Confirm the transaction
            try {
                Log.e("FinalMessage", mPrintedData);
                if (mPrintedData.length() > 0) {
                    erro = false;
                    String newReturn = "SUCESSO: " + mPrintedData + ";DATA_ADICIONAL " + aditionalData;
                    Log.e("FinalMessage", newReturn);
                    returnData( newReturn );
                    clisitef.finishTransaction(1);
                } else {

                    if(g_funcao == 121 ){ //--Evento de envio de trace
                        returnData(  "SUCESSO: LOG Enviado com sucesso;");
                        finish();
                    } else{
                        returnData(  "ERRO: Sem dados de retorno;");
                    }
                }

            } catch (Exception e) {
                //alert(e.getMessage());
            }
        } else {
            Log.e("onTransactionResult", "Else 1");
            if (resultCode == 0) {
                if (erro){
                    finish();
                } else {
                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Atenção")
                            .setMessage("Deseja imprimir a via do cliente?")
                            .setCancelable(false)

                            .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    impressao = "Via Cliente" + System.getProperty("line.separator") +  mPrintedData;

                                    Log.e("Impressao", "Antes de chamar");
                                    printResult(impressao);

                                    //Stop the activity
                                    finish();
                                }

                            })
                            .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
            } else {
                Log.e("onTransactionResult", "Else 2");
                returnData(getMessageDescription(stage, resultCode));
                if (clisitef.abortTransaction(-1) != 0) {
                    // Se não há o que abortar, encerra a Activity
                    finish();
                }
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.GET_DATA) {
            if (resultCode == RESULT_OK) {
                String in = "";
                if (data.getExtras() != null) {
                    in = data.getExtras().getString("input");
                }
                clisitef.continueTransaction(in);
            } else if (resultCode == RESULT_CANCELED) {
                clisitef.abortTransaction(-1);
            }
        } else if (requestCode == RequestCode.END_STAGE_1_MSG && trnResultCode != 0) {
            finish();
        } else if (requestCode == RequestCode.END_STAGE_2_MSG) {
            finish();
        }
    }

    private void setMessageTitle(int what) {
        setTitle(getString(what));
    }

    private static Handler hndMessage = new Handler() {
        public void handleMessage(android.os.Message message) {

            Log.e("LOG:", "Handler: " + message);
            switch (message.what) {
                case CliSiTef.EVT_BEGIN_PP_CONNECT:
                    instance.setProgressBarIndeterminateVisibility(true);
                    instance.setMessageTitle(R.string.msgPPSearching);
                    break;
                case CliSiTef.EVT_END_PP_CONNECT:
                    instance.setProgressBarIndeterminateVisibility(false);
                    instance.setTitle(R.string.app_name);
                    break;
                case CliSiTef.EVT_BEGIN_PP_CONFIG:
                    instance.setProgressBarIndeterminateVisibility(true);
                    instance.setMessageTitle(R.string.msgPPConfiguring);
                    break;
                case CliSiTef.EVT_END_PP_CONFIG:
                    instance.setProgressBarIndeterminateVisibility(false);
                    instance.setMessageTitle(R.string.msgPPConfigured);
                    break;
                case CliSiTef.EVT_BT_PP_DISCONNECT:
//                    instance.setProgressBarIndeterminateVisibility(false);
                    //                  instance.setMessageTitle(R.string.msgPPDisconnected);
                    break;
            }
        }
    };

    private void verificaParametros(){
        Log.e("viaCliente", "Antes de verificar os parametros");


        if (getIntent().getExtras() != null) {
            Log.e("PARAMs", "1");
            g_ip = getIntent().getStringExtra("g_ip");
            g_funcao = Integer.parseInt(getIntent().getStringExtra("g_funcao"));
            g_parametros = getIntent().getStringExtra("g_parametros");
            g_empresa = getIntent().getStringExtra("g_empresa");
            g_data = getIntent().getStringExtra("g_data");
            g_hora = getIntent().getStringExtra("g_hora");
            g_cupom = getIntent().getStringExtra("g_cupom");

            switch (g_funcao) {
                case 115: // Solicitação da Pré Reserva
                    p_valor = getIntent().getStringExtra("p_valor");
                    p_operador = getIntent().getStringExtra("p_operador");

                    break;
                case 116: //-- Confirmação da Pré Reserva
                    p_valor = getIntent().getStringExtra("p_valor");
                    break;
                case 202: //-- Cancelamento da Pré Reserva
                    Log.e("PARAMs", "3");
                    cg_data = g_data.substring(6,8) + g_data.substring(4,6) +g_data.substring(0,4);
                    Log.e("PARAMs", cg_data);

                    cp_valor = getIntent().getStringExtra("cp_valor");;
                    Log.e("PARAMs", "32");
                    cp_nsu = getIntent().getStringExtra("cp_nsu");
                    Log.e("PARAMs", "33");
                    cp_autorizacao = getIntent().getStringExtra("cp_autorizacao");
                    Log.e("PARAMs", "4");
                    break;
            }

        }else{
            returnData("ERRO: Sem parametros.");
        }

    }

    private void printResult(String data){

        try {
            iprntr = iGedi.getPRNTR();
            iprntr.Init();
            GEDI_PRNTR_st_StringConfig strconfig = new GEDI_PRNTR_st_StringConfig(new Paint());
            strconfig.paint.setTypeface(Typeface.MONOSPACE);
            strconfig.paint.setTextSize(16);
            String spaces = System.getProperty("line.separator") + System.getProperty("line.separator") + System.getProperty("line.separator") + System.getProperty("line.separator") + System.getProperty("line.separator");
            iprntr.DrawStringExt(strconfig, data+spaces);
            iprntr.Output();

            mPrintedData = data;

        } catch (GediException e) {
            e.printStackTrace();
        }

    }

    private void returnData(String errorMsg) {
        try {
            Intent data = new Intent();
            data.putExtra("retorno", errorMsg);
            setResult(Activity.RESULT_OK, data);

            Log.e("FinalMessage", mPrintedData);
            clisitef.abortTransaction(-1);

        } catch(Exception ex){}
    }

    private void solicitaInformacao(String mensagem) {
        Intent i = new Intent(getApplicationContext(), DialogActivity.class);
        i.putExtra("title", title);
        i.putExtra("message", clisitef.getBuffer());
        startActivityForResult(i, RequestCode.GET_DATA);
    }

    private String deviceId (){

        String fileName = Environment.getExternalStoragePublicDirectory("GERTEC") + "/deviceIdentification";
        String id = "";

        id = LerArquivo(fileName);

        if (id.trim().length() == 0 ){
            id = GravaArquivo(fileName);
        }

        return id;
    }

    private String LerArquivo(String fileName) {
        String content = "";
        try {
            FileInputStream fis = new FileInputStream(fileName);
            DataInputStream in = new DataInputStream(fis);
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(in));
            content = br.readLine();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }

    private String GravaArquivo(String fileName){
        int random = new Random().nextInt(99999) + 900000;
        String id = "GE" + random;
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "GERTEC");
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    Log.e("TravellerLog :: ", "Problem creating Image folder");
                    return "";
                }
            }

            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(id.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return id;
    }
}



