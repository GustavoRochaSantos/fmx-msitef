package com.example.fmxpagto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import androidx.annotation.NonNull;

import com.artech.actions.ApiAction;
import com.artech.externalapi.ExternalApi;
import com.artech.externalapi.ExternalApiResult;

public class FmxMCliSitef extends ExternalApi {
	final static String NAME = "FmxMCliSitef";
	private final static String METHOD_GPOS700_PRERESERVA = "PreReserva";
	private final static String METHOD_GPOS700_CONFIRMACAOPRERESERVA = "ConfirmarPreReserva";
	private final static String METHOD_GPOS700_CANCELARPRERESERVA = "CancelarPreReserva";
	private final static String METHOD_GPOS700_VALIDACERTIFICADO = "ValidaCertificado";

	private int RC_OCR_CAPTURE = 777;

	public FmxMCliSitef(ApiAction action) {
		super(action);
		addMethodHandler(METHOD_GPOS700_PRERESERVA, 9, mMethodPreReserva);
		addMethodHandler(METHOD_GPOS700_CONFIRMACAOPRERESERVA, 8, mMethodConfirmarPreReserva);
		addMethodHandler(METHOD_GPOS700_CANCELARPRERESERVA, 10, mMethodCancelarPreReserva);
		addMethodHandler(METHOD_GPOS700_VALIDACERTIFICADO, 2, mMethodValidaCertificado);
	}

	private final IMethodInvoker mMethodPreReserva = new IMethodInvoker() {
		@Override
		public @NonNull ExternalApiResult invoke(List<Object> parameters) {
			//Services.Messages.showMessage(getContext().getString(com.example.genexusmodule.R.string.hello_message));

			if(parameters.get(0).equals("")){
				return ExternalApiResult.success("ERRO: O IP deve ser informado!;");
			}

			Log.e("NOBRE", "Parametro1");
			if(parameters.get(1).equals("")){
				return ExternalApiResult.success("ERRO: A FUNÇÃO deve ser informada!;");

			}
			Log.e("NOBRE", "Parametro2");
			if(parameters.get(2).equals("")){
				return ExternalApiResult.success("ERRO: A EMPRESA deve ser informada!;");

			}
			Log.e("NOBRE", "Parametro3");
			if(parameters.get(3).equals("")){
				return ExternalApiResult.success("ERRO: O PARAMETRO deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro4");
			if(parameters.get(4).equals("")){
				return ExternalApiResult.success("ERRO: O VALOR deve ser informado!;");

			}
			Log.e("NOBRE", "Parametro5");
			if(parameters.get(5).equals("")){
				return ExternalApiResult.success("ERRO: A DATA deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro6");
			if(parameters.get(6).equals("")){
				return ExternalApiResult.success("ERRO: A HORA deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro7");
			if(parameters.get(7).equals("")){
				return ExternalApiResult.success("ERRO: O USUARIO deve ser informado!;");

			}
			Log.e("NOBRE", "Parametro8");
			if(parameters.get(8).equals("")){
				return ExternalApiResult.success("ERRO: O CUPOM deve ser informado!;");
			}
			Log.e("NOBRE", "Inicio PutExtra");

			Intent intent = new Intent(getActivity(), TransactionActivity.class);

			intent.putExtra("g_ip", (String) parameters.get(0));
			intent.putExtra("g_funcao", (String) parameters.get(1));
			intent.putExtra("g_empresa", (String) parameters.get(2));
			intent.putExtra("g_parametros", (String) parameters.get(3));

			intent.putExtra("p_valor", (String) parameters.get(4));
			intent.putExtra("g_data", (String) parameters.get(5));
			intent.putExtra("g_hora", (String) parameters.get(6));
			intent.putExtra("p_operador", (String) parameters.get(7));
			intent.putExtra("g_cupom", (String) parameters.get(8));
			Log.e("NOBRE", "Final PutExtra");

			startActivityForResult(intent, RC_OCR_CAPTURE);
			return ExternalApiResult.SUCCESS_WAIT;
		}
	};

	private final IMethodInvoker mMethodConfirmarPreReserva = new IMethodInvoker() {
		@Override
		public @NonNull ExternalApiResult invoke(List<Object> parameters) {
			//Services.Messages.showMessage(getContext().getString(com.example.genexusmodule.R.string.hello_message));

			if(parameters.get(0).equals("")){
				return ExternalApiResult.success("ERRO: O IP deve ser informado!;");
			}

			Log.e("NOBRE", "Parametro1");
			if(parameters.get(1).equals("")){
				return ExternalApiResult.success("ERRO: A FUNÇÃO deve ser informada!;");

			}
			Log.e("NOBRE", "Parametro2");
			if(parameters.get(2).equals("")){
				return ExternalApiResult.success("ERRO: A EMPRESA deve ser informada!;");

			}
			Log.e("NOBRE", "Parametro3");
			if(parameters.get(3).equals("")){
				return ExternalApiResult.success("ERRO: O PARAMETRO deve ser informado!;");
			}

			if(parameters.get(4).equals("")){
				return ExternalApiResult.success("ERRO: A DATA deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro1");
			if(parameters.get(5).equals("")){
				return ExternalApiResult.success("ERRO: A HORA deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro1");
			if(parameters.get(6).equals("")){
				return ExternalApiResult.success("ERRO: O CUPOM deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro1");
			if(parameters.get(7).equals("")){
				return ExternalApiResult.success("ERRO: O VALOR deve ser informado!;");
			}

			Log.e("Cupom", (String) parameters.get(6));
			Intent intent = new Intent(getActivity(), TransactionActivity.class);
			intent.putExtra("g_ip", (String) parameters.get(0));
			intent.putExtra("g_funcao", (String) parameters.get(1));
			intent.putExtra("g_empresa", (String) parameters.get(2));
			intent.putExtra("g_parametros", (String) parameters.get(3));

			intent.putExtra("g_data", (String) parameters.get(4));
			intent.putExtra("g_hora", (String) parameters.get(5));
			intent.putExtra("g_cupom", (String) parameters.get(6));
			intent.putExtra("p_valor", (String) parameters.get(7));

			startActivityForResult(intent, RC_OCR_CAPTURE);
			return ExternalApiResult.SUCCESS_WAIT;
		}
	};

	private final IMethodInvoker mMethodCancelarPreReserva = new IMethodInvoker() {
		@Override
		public @NonNull ExternalApiResult invoke(List<Object> parameters) {
			//Services.Messages.showMessage(getContext().getString(com.example.genexusmodule.R.string.hello_message));
			Log.e("NOBRE", "Parametro0");
			if(parameters.get(0).equals("")){
				return ExternalApiResult.success("ERRO: O IP deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro1");
			if(parameters.get(1).equals("")){
				return ExternalApiResult.success("ERRO: A FUNÇÃO deve ser informada!;");

			}
			Log.e("NOBRE", "Parametro2");
			if(parameters.get(2).equals("")){
				return ExternalApiResult.success("ERRO: A EMPRESA deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro3");
			if(parameters.get(3).equals("")){
				return ExternalApiResult.success("ERRO: O PARAMETRO deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro4");
			if(parameters.get(4).equals("")){
				return ExternalApiResult.success("ERRO: A DATA deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro5");
			if(parameters.get(5).equals("")){
				return ExternalApiResult.success("ERRO: A HORA deve ser informado!;");
			}
			Log.e("NOBRE", "Parametro6");
			if(parameters.get(6).equals("")){
				return ExternalApiResult.success("ERRO: O CUPOM deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro7");
			if(parameters.get(7).equals("")){
				return ExternalApiResult.success("ERRO: O VALOR deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro8");
			if(parameters.get(8).equals("")){
				return ExternalApiResult.success("ERRO: O NSU deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro9");
			if(parameters.get(9).equals("")){
				return ExternalApiResult.success("ERRO: O CÓD DE AUTORIZAÇÃO deve ser informada!;");
			}

			Intent intent = new Intent(getActivity(), TransactionActivity.class);
			intent.putExtra("g_ip", (String) parameters.get(0));
			intent.putExtra("g_funcao", (String) parameters.get(1));
			intent.putExtra("g_empresa", (String) parameters.get(2));
			intent.putExtra("g_parametros", (String) parameters.get(3));

			intent.putExtra("g_data", (String) parameters.get(4));
			intent.putExtra("g_hora", (String) parameters.get(5));
			intent.putExtra("g_cupom", (String) parameters.get(6));

			intent.putExtra("cp_valor", (String) parameters.get(7));
			intent.putExtra("cp_nsu", (String) parameters.get(8));
			intent.putExtra("cp_autorizacao", (String) parameters.get(9));
			Log.e("NOBRE", "Final PutExtra");

			startActivityForResult(intent, RC_OCR_CAPTURE);
			return ExternalApiResult.SUCCESS_WAIT;
		}
	};

	private final IMethodInvoker mMethodValidaCertificado = new IMethodInvoker() {
		@Override
		public @NonNull ExternalApiResult invoke(List<Object> parameters) {
			//Services.Messages.showMessage(getContext().getString(com.example.genexusmodule.R.string.hello_message));
			Log.e("NOBRE", "Parametro0");
			if(parameters.get(0).equals("")){
				return ExternalApiResult.success("ERRO: A URL deve ser informada!;");
			}
			Log.e("NOBRE", "Parametro0");
			if(parameters.get(1).equals("")){
				return ExternalApiResult.success("ERRO: O NOME DO ARQUIVO deve ser informado!;");
			}

			String urlSite = (String) parameters.get(0);
			String nomeArquivo = (String) parameters.get(1);

			String MsgRetorno = DownloadFromURL(urlSite, nomeArquivo);

			return ExternalApiResult.success(MsgRetorno);
		}
	};


	@Override
	public @NonNull ExternalApiResult afterActivityResult(int requestCode, int resultCode, Intent result, String method, List<Object> methodParameters)
	{
		String returnString = "Erro: Sem retorno";//String.valueOf(OPEN_REQUEST_FOLDER);
		Log.e("NOBRE", returnString);

		//		Log.e("NOBRE", globalmethod);
		if (resultCode == Activity.RESULT_OK)
		{
//			if (globalmethod.equalsIgnoreCase(METHOD_DRAWIMAGE)) {
			if (result != null) {
				String text = result.getStringExtra("retorno");
				Log.e("RETORNO", text);
				returnString = text;
			}
//			}
		}
		return ExternalApiResult.success(returnString);

	}

	public String DownloadFromURL(String urlSite, String nomeArquivo) {
		try {
			URL url = new URL(urlSite);
			HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod("GET");
			conexao.connect();
			if (conexao.getResponseCode() == HttpURLConnection.HTTP_OK) {
				Log.e("DownloadFromURL", "Conexao realizada com sucesso !");
			}
			// Verificar se o arquivo já existe. Se sim, apagá-lo...

			try {
				File file = new File(getContext().getFilesDir(), nomeArquivo);

				if(file.exists()){
					Log.e("DownloadFromURL", "Arquivo existente!");

					file.getCanonicalFile().delete();
					if(file.exists()){
						getContext().deleteFile(file.getName());
						Log.e("DownloadFromURL", "Arquivo existente apagado com sucesso !");
					}

				}


			} catch (Exception excecao) {
				Log.e("DownloadFromURL", "Falha ao apagar o arquivo antigo " + nomeArquivo + " - " + excecao);
				return "ERRO: Não foi possível apagar o certificado antigo!";
			}


			FileOutputStream fos = getContext().openFileOutput(nomeArquivo, Context.MODE_PRIVATE);
			InputStream is = conexao.getInputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = is.read(buffer)) != -1) {
				Log.e("DownloadFromURL", "Lendo dados do http...");
				fos.write(buffer, 0, len);
			}
			fos.close();
			is.close();
			Log.e("DownloadFromURL", "Arquivo baixado ");

			return "SUCCESS: Certificado Baixado/Atualizado!";


		} catch (Exception excecao) {
			Log.e("DownloadFromURL", "Erro ao efetuar download do arquivo " + nomeArquivo + " - " + excecao);
			return "ERRO: Não foi possível localizar o certificado para download!";
		}

	}



}

